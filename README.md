# AliTwitter
A very basic twitter clone with the following features:
- Create tweets
- Read tweets
- Delete tweets

## Environment Setup
- Ruby 2.6.5
- Bundler 2.1.4
- Rails 6.0.2.1

## System Dependencies
- [Virtualbox](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/downloads.html)
- [Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html)

## Run Instructions

#### 1. Boot up the base virtual machines with Vagrant

```
vagrant up
```
The following virtual machines will be created:

| Name | IP Address | Description |
|-|-|-|
| db | 192.168.50.4 | VM that runs PostgreSQL |
| app1 | 192.168.50.5 | VM that runs the app replica |
| app2 | 192.168.50.6 | VM that runs the app replica |
| lb | 192.168.50.7 | VM that handles load balancing with HAProxy  |
| gitlab-runner | 192.168.50.100 | VM that runs gitlab runner |

#### 2. Create the build artifact

Build the project with:
```
bundle config frozen true
bundle config set without development:test
bundle install -j4 --retry 3
yarn install
RAILS_ENV=production SECRET_KEY_BASE=key bundle exec rake assets:precompile
```
Create the build artifact with:
```
tar --exclude=.git --exclude=provision --exclude=node_modules --exclude=tmp/cache --exclude=vendor/assets --exclude=lib/assets --exclude=test -zcvf provision/roles/load/files/alitwitter.tar .
```

#### 3. Prepare the virtual machines
```
ansible-playbook -i provision/hosts/hosts.yml provision/setup_env.yml
```

#### 4. Deploy the virtual machines
```
ansible-playbook -i provision/hosts/hosts.yml provision/deploy.yml
```
The app can then be accessed through your browser at `localhost:3000`

#### 5. (Optional) Control the application through Ansible scripts
| Script | Description |
|-|-|
| setup_env | Prepares the freshly booted VMs with the necessary dependencies |
| deploy | Loads the build artifact and runs the AliTwitter service |
| redeploy | Stops the AliTwitter service and performs `deploy` |
| stop | Stops the AliTwitter service |
| start | Resumes the service if the `stop` script was used |

Usage example (redeploying):
```
ansible-playbook -i provision/hosts/hosts.yml provision/redeploy.yml
```

## CI/CD
To setup the CI/CD pipeline, refer to the [CI/CD documentation](CICD.md).
