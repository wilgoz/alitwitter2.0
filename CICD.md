# CI/CD Setup
This section covers the steps required to setup the CI/CD pipeline for this project. Ensure that all the system dependencies covered in the README have been satisfied.

#### 1. Boot up the base virtual machines with Vagrant

```
vagrant up
```
The following virtual machines will be created:

| Name | IP Address | Description |
|-|-|-|
| db | 192.168.50.4 | VM that runs PostgreSQL |
| app1 | 192.168.50.5 | VM that runs the app replica |
| app2 | 192.168.50.6 | VM that runs the app replica |
| lb | 192.168.50.7 | VM that handles load balancing with HAProxy  |
| gitlab-runner | 192.168.50.100 | VM that runs gitlab runner |

#### 2. Prepare the virtual machines
In the root project diretory, execute:
```
ansible-playbook -i provision/hosts/hosts.yml provision/setup_env.yml
```

#### 3. SSH into the gitlab-runner VM
Establishing an SSH connection with the gitlab-runner VM can be done through Vagrant, as follows:
```
vagrant ssh gitlab-runner
```
Or manually assuming OpenSSH is installed in your local machine:
```
ssh -i .vagrant/machines/gitlab-runner/virtualbox/private_key vagrant@192.168.50.100
```

#### 4. Set up Docker and Shell executors in the gitlab-runner VM
> The step assumes that you have a fresh gitlab repository that will contain this project.

To register the runner to gitlab, execute:
```
sudo gitlab-runner register --docker-privileged
```

You will be prompted to provide several inputs. Follow the following input sequence to register the Docker executor:

1. https://gitlab.com
2. The gitlab-ci token can be found in your gitlab repository in `Settings > CI / CD > Runners`
3. build-runner
4. docker
5. docker
6. alpine:latest

Repeat the above steps to register the Shell executor with the following modifications (step 6 is no longer required):

3. deploy-runner
4. shell
5. shell

After registering the above executors, execute the following command to restart gitlab-runner:
```
sudo gitlab-runner restart
```

You may logout of the gitlab-runner VM at this stage.

#### 5. Set up private keys in your gitlab repository
Assuming you are currently in the root project directory (or more accurately, the project directory which contains the `Vagrantfile` file), Vagrant will automatically generate the private keys of the created virtual machines in:
```
.vagrant/machines/{VM NAME}/virtualbox/private_key
```

You will need to take note of the private keys for the `app1` and `app2` VMs and this can be done through the use of commands such as `cat`:
```
cat .vagrant/machines/app1/virtualbox/private_key
cat .vagrant/machines/app2/virtualbox/private_key
```

In the `Settings > CI / CD > Variables` section of your gitlab repository,
set up the following variables:

| Type | Key | Value | State | Masked | Scope
|-|-|-|-|-|-|
| File | APP1_PRIV_KEY | { The copied app1 VM private key } | Disabled | Disabled | All environments
| File | APP2_PRIV_KEY | { The copied app2 VM private key }  | Disabled | Disabled | All environments

#### 6. Push the project to your repo
The CI/CD pipeline should automatically run upon every pushes to the `master` branch.